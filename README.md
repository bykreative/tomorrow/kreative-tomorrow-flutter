# Kreative Tomorrow App

The main app for Kreative Tomorrow giving members the ability to earn Kreative Coin, register for events, get real-time updates, make announcements to the rest of the community, and much more!